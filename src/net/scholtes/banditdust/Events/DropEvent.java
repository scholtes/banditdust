package net.scholtes.banditdust.Events;

import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;

import com.sk89q.worldguard.protection.regions.ProtectedRegion;

import net.scholtes.banditdust.DustAPI;

public class DropEvent implements Listener {

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		DustAPI.addDust(0, e);
	}
	
	
	
	@EventHandler
	public void onMerge(ItemMergeEvent e) {
		Location location = e.getTarget().getLocation();
		ProtectedRegion region = DustAPI.manager
				.getRegion("nocombine");
		if (region == null) {
			return;
		}
		if (region.contains(location.getBlockX(), location.getBlockY(), location.getBlockZ())) {
			e.setCancelled(true);
		}
	}

}
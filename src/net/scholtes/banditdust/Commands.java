package net.scholtes.banditdust;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Commands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (cmd.getName().equalsIgnoreCase("givedust")) {
			if (sender.hasPermission("dust.give") || sender.isOp()) {
				if (args.length == 0) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
							"&cPlease specify a player! &8(&c/givedust <player> <dust type> <amount>&8)"));
				} else if (args.length == 1) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
							"&cPlease specify a dust type! &8(&c/givedust <player> <dust type> <amount>&8)"));
				} else if (args.length == 2) {
					sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
							"&cPlease specify an amount! &8(&c/givedust <player> <dust type> <amount>&8)"));
				} else {
					if (Bukkit.getPlayer(args[0]) != null) {
						if (isInteger(args[2]) == true) {
							if (args[1].equals("pickaxe")) {
								ItemStack dust = new ItemStack(Material.SULPHUR, Integer.parseInt(args[2]));
								ItemMeta meta = dust.getItemMeta();
								List<String> dustlore = new ArrayList<String>();
								dustlore.add(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("pickaxedustlore")));
								meta.setLore(dustlore);
								meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("pickaxedustname")));
								dust.setItemMeta(meta);
								Bukkit.getPlayer(args[0]).getInventory().addItem(dust);
								sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"&ayay, &o" + args[0] + "&a got &n" + args[2] + "pickaxe dust&a!"));
							} else if (args[1].equals("tag")) {
								ItemStack dust = new ItemStack(Material.REDSTONE, Integer.parseInt(args[2]));
								ItemMeta meta = dust.getItemMeta();
								List<String> dustlore = new ArrayList<String>();
								dustlore.add(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("tagdustlore")));
								meta.setLore(dustlore);
								meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("tagdustname")));
								dust.setItemMeta(meta);
								Bukkit.getPlayer(args[0]).getInventory().addItem(dust);
								sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"&ayay, &o" + args[0] + "&a got &n" + args[2] + "tag dust&a!"));
							} else if (args[1].equals("armor")) {
								ItemStack dust = new ItemStack(Material.GLOWSTONE_DUST, Integer.parseInt(args[2]));
								ItemMeta meta = dust.getItemMeta();
								List<String> dustlore = new ArrayList<String>();
								dustlore.add(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("armordustlore")));
								meta.setLore(dustlore);
								meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("armordustname")));
								dust.setItemMeta(meta);
								Bukkit.getPlayer(args[0]).getInventory().addItem(dust);
								sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"&ayay, &o" + args[0] + "&a got &n" + args[2] + "armor dust&a!"));
							} else if (args[1].equals("scroll")) {
								ItemStack dust = new ItemStack(Material.SUGAR, Integer.parseInt(args[2]));
								ItemMeta meta = dust.getItemMeta();
								List<String> dustlore = new ArrayList<String>();
								dustlore.add(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("scrolldustlore")));
								meta.setLore(dustlore);
								meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("scrolldustname")));
								dust.setItemMeta(meta);
								Bukkit.getPlayer(args[0]).getInventory().addItem(dust);
								sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"&ayay, &o" + args[0] + "&a got &n" + args[2] + "armor dust&a!"));
							} else {
								sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"&cspecify a dust type that exists... jesus christ"));
							}
						} else {
							sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"&cif that is an integer then please kindly shoot urself"));
						}

					} else {
						sender.sendMessage(
								ChatColor.translateAlternateColorCodes('&', "&cspecify a real player u spaz"));
					}
				}

			}
		}

		// Adds pickaxe to items file
		if (cmd.getName().equalsIgnoreCase("setpickaxe")) {
			Player p = (Player) sender;
			if (sender instanceof Player) {
				if (p.hasPermission("dust.set")) {
					ItemStack hand = new ItemStack(p.getItemInHand());
					if (hand.getType().equals(Material.DIAMOND_PICKAXE)) {
						if (args.length == 0 || args.length == 1) {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"&cPlease specify a minimum and maximum amount!"));
						} else {
							if (isInteger(args[0]) == true && isInteger(args[1]) == true) {

								if (p.getItemInHand().getItemMeta().getLore() != null) {
									File data = new File("plugins/BanditDust/", "items.yml");
									FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
									if (data.exists()) {
										int counter = cfg.getConfigurationSection("items").getKeys(false).size() + 1;
										if (cfg.getString("items." + counter) == null) {
											cfg.set("items." + counter + ".itemstack", hand);
											cfg.set("items." + counter + ".min", args[0]);
											cfg.set("items." + counter + ".max", args[1]);
											cfg.set("items." + counter + ".type", "pickaxe");
											try {
												cfg.save(data);
											} catch (IOException e1) {
												e1.printStackTrace();
											}
										}
									} else if (!data.exists()) {
										cfg.set("items." + 1 + ".itemstack", hand);
										cfg.set("items." + 1 + ".min", args[0]);
										cfg.set("items." + 1 + ".max", args[1]);
										cfg.set("items." + 1 + ".type", "pickaxe");
										try {
											cfg.save(data);
										} catch (IOException e1) {
											e1.printStackTrace();
										}
									}
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"" + Main.getInstance().getConfig().getString("prefix")
													+ " &2Item added! It can now be found in items.yml"));
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"&8-----------------------------"));
									p.sendMessage(
											ChatColor.translateAlternateColorCodes('&', "&aDust Type: &oPickaxe"));
									p.sendMessage(
											ChatColor.translateAlternateColorCodes('&', "&aMin Dust: &o" + args[0]));
									p.sendMessage(
											ChatColor.translateAlternateColorCodes('&', "&aMax Dust: &o" + args[1]));
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"&8-----------------------------"));
								} else {
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"" + Main.getInstance().getConfig().getString("prefix")
													+ "&cThe item needs to have a lore!"));
								}
							} else {
								p.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("prefix")
												+ "&cPlease specify an integer for minimum and maximum amount!"));
							}
						}
					} else {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"" + Main.getInstance().getConfig().getString("prefix")
										+ " &cYou need to be holding a pickaxe!"));
					}

				}
			} else if (!(sender instanceof Player)) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("prefix")
								+ " &cYou need to be a player to do this!"));
			}
		}

		// Removes pickaxe from items file
		if (cmd.getName().equalsIgnoreCase("removepickaxe")) {
			Player p = (Player) sender;
			if (sender instanceof Player) {
				if (p.hasPermission("dust.set")) {
					ItemStack hand = new ItemStack(p.getItemInHand());
					if (hand.getType().equals(Material.DIAMOND_PICKAXE)) {
						File data = new File("plugins/BanditDust/", "items.yml");
						FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
						int counter = cfg.getConfigurationSection("items").getKeys(false).size();
						if (counter != 0) {
							for (int count = 1; count <= counter; count++) {
								ItemStack cfgitem = cfg.getItemStack("items." + count + ".itemstack");
								if (cfgitem.equals(hand)) {
									cfg.set("items." + count, null);
									try {
										cfg.save(data);
									} catch (IOException e1) {
										e1.printStackTrace();
									}
									p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""
											+ Main.getInstance().getConfig().getString("prefix") + " &2Item removed!"));
									break;
								} else if (count == counter) {
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"" + Main.getInstance().getConfig().getString("prefix")
													+ " &cItem not found!"));
								}
							}
						} else {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"" + Main.getInstance().getConfig().getString("prefix")
											+ " &cThere is no items in the config!"));
						}

					} else {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"" + Main.getInstance().getConfig().getString("prefix")
										+ " &cYou need to be holding a pickaxe!"));
					}

				}
			} else if (!(sender instanceof Player)) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("prefix")
								+ " &cYou need to be a player to do this!"));
			}
		}

		// Adds tag to items file
		if (cmd.getName().equalsIgnoreCase("settag")) {
			Player p = (Player) sender;
			if (sender instanceof Player) {
				if (p.hasPermission("dust.set")) {
					ItemStack hand = new ItemStack(p.getItemInHand());
					if (hand.getType().equals(Material.NAME_TAG)) {
						if (args.length == 0 || args.length == 1) {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"&cPlease specify a minimum and maximum amount!"));
						} else {
							if (isInteger(args[0]) == true && isInteger(args[1]) == true) {

								if (p.getItemInHand().getItemMeta().getLore() != null) {
									File data = new File("plugins/BanditDust/", "items.yml");
									FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
									if (data.exists()) {
										int counter = cfg.getConfigurationSection("items").getKeys(false).size() + 1;
										if (cfg.getString("items." + counter) == null) {
											cfg.set("items." + counter + ".itemstack", hand);
											cfg.set("items." + counter + ".min", args[0]);
											cfg.set("items." + counter + ".max", args[1]);
											cfg.set("items." + counter + ".type", "tag");
											try {
												cfg.save(data);
											} catch (IOException e1) {
												e1.printStackTrace();
											}
										}
									} else if (!data.exists()) {
										cfg.set("items." + 1 + ".itemstack", hand);
										cfg.set("items." + 1 + ".min", args[0]);
										cfg.set("items." + 1 + ".max", args[1]);
										cfg.set("items." + 1 + ".type", "tag");
										try {
											cfg.save(data);
										} catch (IOException e1) {
											e1.printStackTrace();
										}
									}
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"" + Main.getInstance().getConfig().getString("prefix")
													+ " &2Item added! It can now be found in items.yml"));
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"&8-----------------------------"));
									p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aDust Type: &oTag"));
									p.sendMessage(
											ChatColor.translateAlternateColorCodes('&', "&aMin Dust: &o" + args[0]));
									p.sendMessage(
											ChatColor.translateAlternateColorCodes('&', "&aMax Dust: &o" + args[1]));
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"&8-----------------------------"));
								} else {
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"" + Main.getInstance().getConfig().getString("prefix")
													+ "&cThe item needs to have a lore!"));
								}
							} else {
								p.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("prefix")
												+ "&cPlease specify an integer for minimum and maximum amount!"));
							}
						}
					} else {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"" + Main.getInstance().getConfig().getString("prefix")
										+ " &cYou need to be holding a tag!"));
					}

				}
			} else if (!(sender instanceof Player)) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("prefix")
								+ " &cYou need to be a player to do this!"));
			}
		}

		// Removes tag from items file
		if (cmd.getName().equalsIgnoreCase("removetag")) {
			Player p = (Player) sender;
			if (sender instanceof Player) {
				if (p.hasPermission("dust.set")) {
					ItemStack hand = new ItemStack(p.getItemInHand());
					if (hand.getType().equals(Material.NAME_TAG)) {
						File data = new File("plugins/BanditDust/", "items.yml");
						FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
						int counter = cfg.getConfigurationSection("items").getKeys(false).size();
						if (counter != 0) {
							for (int count = 1; count <= counter; count++) {
								ItemStack cfgitem = cfg.getItemStack("items." + count + ".itemstack");
								if (cfgitem.equals(hand)) {
									cfg.set("items." + count, null);
									try {
										cfg.save(data);
									} catch (IOException e1) {
										e1.printStackTrace();
									}
									p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""
											+ Main.getInstance().getConfig().getString("prefix") + " &2Item removed!"));
									break;
								} else if (count == counter) {
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"" + Main.getInstance().getConfig().getString("prefix")
													+ " &cItem not found!"));
								}
							}
						} else {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"" + Main.getInstance().getConfig().getString("prefix")
											+ " &cThere is no items in the config!"));
						}

					} else {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"" + Main.getInstance().getConfig().getString("prefix")
										+ " &cYou need to be holding a tag!"));
					}

				}
			} else if (!(sender instanceof Player)) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("prefix")
								+ " &cYou need to be a player to do this!"));
			}
		}

		// Adds armor to items file
		if (cmd.getName().equalsIgnoreCase("setarmor")) {
			Player p = (Player) sender;
			if (sender instanceof Player) {
				if (p.hasPermission("dust.set")) {
					ItemStack hand = new ItemStack(p.getItemInHand());
					if (hand.getType().equals(Material.CHEST) || hand.getType().equals(Material.ENDER_CHEST)) {
						if (args.length == 0 || args.length == 1) {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"&cPlease specify a minimum and maximum amount!"));
						} else {
							if (isInteger(args[0]) == true && isInteger(args[1]) == true) {

								if (p.getItemInHand().getItemMeta().getLore() != null) {
									File data = new File("plugins/BanditDust/", "items.yml");
									FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
									if (data.exists()) {
										int counter = cfg.getConfigurationSection("items").getKeys(false).size() + 1;
										if (cfg.getString("items." + counter) == null) {
											cfg.set("items." + counter + ".itemstack", hand);
											cfg.set("items." + counter + ".min", args[0]);
											cfg.set("items." + counter + ".max", args[1]);
											cfg.set("items." + counter + ".type", "armor");
											try {
												cfg.save(data);
											} catch (IOException e1) {
												e1.printStackTrace();
											}
										}
									} else if (!data.exists()) {
										cfg.set("items." + 1 + ".itemstack", hand);
										cfg.set("items." + 1 + ".min", args[0]);
										cfg.set("items." + 1 + ".max", args[1]);
										cfg.set("items." + 1 + ".type", "armor");
										try {
											cfg.save(data);
										} catch (IOException e1) {
											e1.printStackTrace();
										}
									}
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"" + Main.getInstance().getConfig().getString("prefix")
													+ " &2Item added! It can now be found in items.yml"));
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"&8-----------------------------"));
									p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aDust Type: &oArmor"));
									p.sendMessage(
											ChatColor.translateAlternateColorCodes('&', "&aMin Dust: &o" + args[0]));
									p.sendMessage(
											ChatColor.translateAlternateColorCodes('&', "&aMax Dust: &o" + args[1]));
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"&8-----------------------------"));
								} else {
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"" + Main.getInstance().getConfig().getString("prefix")
													+ "&cThe item needs to have a lore!"));
								}
							} else {
								p.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("prefix")
												+ "&cPlease specify an integer for minimum and maximum amount!"));
							}
						}
					} else {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"" + Main.getInstance().getConfig().getString("prefix")
										+ " &cYou need to be holding an armor set!"));
					}

				}
			} else if (!(sender instanceof Player)) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("prefix")
								+ " &cYou need to be a player to do this!"));
			}
		}

		// Removes armor from items file
		if (cmd.getName().equalsIgnoreCase("removearmor")) {
			Player p = (Player) sender;
			if (sender instanceof Player) {
				if (p.hasPermission("dust.set")) {
					ItemStack hand = new ItemStack(p.getItemInHand());
					if (hand.getType().equals(Material.CHEST) || hand.getType().equals(Material.ENDER_CHEST)) {
						File data = new File("plugins/BanditDust/", "items.yml");
						FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
						int counter = cfg.getConfigurationSection("items").getKeys(false).size();
						if (counter != 0) {
							for (int count = 1; count <= counter; count++) {
								ItemStack cfgitem = cfg.getItemStack("items." + count + ".itemstack");
								if (cfgitem.equals(hand)) {
									cfg.set("items." + count, null);
									try {
										cfg.save(data);
									} catch (IOException e1) {
										e1.printStackTrace();
									}
									p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""
											+ Main.getInstance().getConfig().getString("prefix") + " &2Item removed!"));
									break;
								} else if (count == counter) {
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"" + Main.getInstance().getConfig().getString("prefix")
													+ " &cItem not found!"));
								}
							}
						} else {
							p.sendMessage(ChatColor.translateAlternateColorCodes('&',
									"" + Main.getInstance().getConfig().getString("prefix")
											+ " &cThere is no items in the config!"));
						}

					} else {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"" + Main.getInstance().getConfig().getString("prefix")
										+ " &cYou need to be holding an armor set!"));
					}

				}
			} else if (!(sender instanceof Player)) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("prefix")
								+ " &cYou need to be a player to do this!"));
			}
		}
		
		// Adds scroll to items file
				if (cmd.getName().equalsIgnoreCase("setscroll")) {
					Player p = (Player) sender;
					if (sender instanceof Player) {
						if (p.hasPermission("dust.set")) {
							ItemStack hand = new ItemStack(p.getItemInHand());
							if (hand.getType().equals(Material.PAPER)) {
								if (args.length == 0 || args.length == 1) {
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"&cPlease specify a minimum and maximum amount!"));
								} else {
									if (isInteger(args[0]) == true && isInteger(args[1]) == true) {

										if (p.getItemInHand().getItemMeta().getLore() != null) {
											File data = new File("plugins/BanditDust/", "items.yml");
											FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
											if (data.exists()) {
												int counter = cfg.getConfigurationSection("items").getKeys(false).size() + 1;
												if (cfg.getString("items." + counter) == null) {
													cfg.set("items." + counter + ".itemstack", hand);
													cfg.set("items." + counter + ".min", args[0]);
													cfg.set("items." + counter + ".max", args[1]);
													cfg.set("items." + counter + ".type", "scroll");
													try {
														cfg.save(data);
													} catch (IOException e1) {
														e1.printStackTrace();
													}
												}
											} else if (!data.exists()) {
												cfg.set("items." + 1 + ".itemstack", hand);
												cfg.set("items." + 1 + ".min", args[0]);
												cfg.set("items." + 1 + ".max", args[1]);
												cfg.set("items." + 1 + ".type", "scroll");
												try {
													cfg.save(data);
												} catch (IOException e1) {
													e1.printStackTrace();
												}
											}
											p.sendMessage(ChatColor.translateAlternateColorCodes('&',
													"" + Main.getInstance().getConfig().getString("prefix")
															+ " &2Item added! It can now be found in items.yml"));
											p.sendMessage(ChatColor.translateAlternateColorCodes('&',
													"&8-----------------------------"));
											p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aDust Type: &oScroll"));
											p.sendMessage(
													ChatColor.translateAlternateColorCodes('&', "&aMin Dust: &o" + args[0]));
											p.sendMessage(
													ChatColor.translateAlternateColorCodes('&', "&aMax Dust: &o" + args[1]));
											p.sendMessage(ChatColor.translateAlternateColorCodes('&',
													"&8-----------------------------"));
										} else {
											p.sendMessage(ChatColor.translateAlternateColorCodes('&',
													"" + Main.getInstance().getConfig().getString("prefix")
															+ "&cThe item needs to have a lore!"));
										}
									} else {
										p.sendMessage(ChatColor.translateAlternateColorCodes('&',
												"" + Main.getInstance().getConfig().getString("prefix")
														+ "&cPlease specify an integer for minimum and maximum amount!"));
									}
								}
							} else {
								p.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("prefix")
												+ " &cYou need to be holding a scroll!"));
							}

						}
					} else if (!(sender instanceof Player)) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"" + Main.getInstance().getConfig().getString("prefix")
										+ " &cYou need to be a player to do this!"));
					}
				}

				// Removes scroll from items file
				if (cmd.getName().equalsIgnoreCase("removescroll")) {
					Player p = (Player) sender;
					if (sender instanceof Player) {
						if (p.hasPermission("dust.set")) {
							ItemStack hand = new ItemStack(p.getItemInHand());
							if (hand.getType().equals(Material.PAPER)) {
								File data = new File("plugins/BanditDust/", "items.yml");
								FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
								int counter = cfg.getConfigurationSection("items").getKeys(false).size();
								if (counter != 0) {
									for (int count = 1; count <= counter; count++) {
										ItemStack cfgitem = cfg.getItemStack("items." + count + ".itemstack");
										if (cfgitem.equals(hand)) {
											cfg.set("items." + count, null);
											try {
												cfg.save(data);
											} catch (IOException e1) {
												e1.printStackTrace();
											}
											p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""
													+ Main.getInstance().getConfig().getString("prefix") + " &2Item removed!"));
											break;
										} else if (count == counter) {
											p.sendMessage(ChatColor.translateAlternateColorCodes('&',
													"" + Main.getInstance().getConfig().getString("prefix")
															+ " &cItem not found!"));
										}
									}
								} else {
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"" + Main.getInstance().getConfig().getString("prefix")
													+ " &cThere is no items in the config!"));
								}

							} else {
								p.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("prefix")
												+ " &cYou need to be holding a scroll!"));
							}

						}
					} else if (!(sender instanceof Player)) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"" + Main.getInstance().getConfig().getString("prefix")
										+ " &cYou need to be a player to do this!"));
					}
				}
				
				// Adds scroll to items file
				if (cmd.getName().equalsIgnoreCase("setpet")) {
					Player p = (Player) sender;
					if (sender instanceof Player) {
						if (p.hasPermission("dust.set")) {
							ItemStack hand = new ItemStack(p.getItemInHand());
								if (args.length == 0 || args.length == 1) {
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"&cPlease specify a minimum and maximum amount!"));
								} else {
									if (isInteger(args[0]) == true && isInteger(args[1]) == true) {

										if (p.getItemInHand().getItemMeta().getLore() != null) {
											File data = new File("plugins/BanditDust/", "items.yml");
											FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
											if (data.exists()) {
												int counter = cfg.getConfigurationSection("items").getKeys(false).size() + 1;
												if (cfg.getString("items." + counter) == null) {
													cfg.set("items." + counter + ".itemstack", hand);
													cfg.set("items." + counter + ".min", args[0]);
													cfg.set("items." + counter + ".max", args[1]);
													cfg.set("items." + counter + ".type", "pet");
													try {
														cfg.save(data);
													} catch (IOException e1) {
														e1.printStackTrace();
													}
												}
											} else if (!data.exists()) {
												cfg.set("items." + 1 + ".itemstack", hand);
												cfg.set("items." + 1 + ".min", args[0]);
												cfg.set("items." + 1 + ".max", args[1]);
												cfg.set("items." + 1 + ".type", "pet");
												try {
													cfg.save(data);
												} catch (IOException e1) {
													e1.printStackTrace();
												}
											}
											p.sendMessage(ChatColor.translateAlternateColorCodes('&',
													"" + Main.getInstance().getConfig().getString("prefix")
															+ " &2Item added! It can now be found in items.yml"));
											p.sendMessage(ChatColor.translateAlternateColorCodes('&',
													"&8-----------------------------"));
											p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aDust Type: &oPet"));
											p.sendMessage(
													ChatColor.translateAlternateColorCodes('&', "&aMin Dust: &o" + args[0]));
											p.sendMessage(
													ChatColor.translateAlternateColorCodes('&', "&aMax Dust: &o" + args[1]));
											p.sendMessage(ChatColor.translateAlternateColorCodes('&',
													"&8-----------------------------"));
										} else {
											p.sendMessage(ChatColor.translateAlternateColorCodes('&',
													"" + Main.getInstance().getConfig().getString("prefix")
															+ "&cThe item needs to have a lore!"));
										}
									} else {
										p.sendMessage(ChatColor.translateAlternateColorCodes('&',
												"" + Main.getInstance().getConfig().getString("prefix")
														+ "&cPlease specify an integer for minimum and maximum amount!"));
									}
								}

						}
					} else if (!(sender instanceof Player)) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"" + Main.getInstance().getConfig().getString("prefix")
										+ " &cYou need to be a player to do this!"));
					}
				}

				// Removes pet from items file
				if (cmd.getName().equalsIgnoreCase("removepet")) {
					Player p = (Player) sender;
					if (sender instanceof Player) {
						if (p.hasPermission("dust.set")) {
							ItemStack hand = new ItemStack(p.getItemInHand());
								File data = new File("plugins/BanditDust/", "items.yml");
								FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
								int counter = cfg.getConfigurationSection("items").getKeys(false).size();
								if (counter != 0) {
									for (int count = 1; count <= counter; count++) {
										ItemStack cfgitem = cfg.getItemStack("items." + count + ".itemstack");
										if (cfgitem.equals(hand)) {
											cfg.set("items." + count, null);
											try {
												cfg.save(data);
											} catch (IOException e1) {
												e1.printStackTrace();
											}
											p.sendMessage(ChatColor.translateAlternateColorCodes('&', ""
													+ Main.getInstance().getConfig().getString("prefix") + " &2Item removed!"));
											break;
										} else if (count == counter) {
											p.sendMessage(ChatColor.translateAlternateColorCodes('&',
													"" + Main.getInstance().getConfig().getString("prefix")
															+ " &cItem not found!"));
										}
									}
								} else {
									p.sendMessage(ChatColor.translateAlternateColorCodes('&',
											"" + Main.getInstance().getConfig().getString("prefix")
													+ " &cThere is no items in the config!"));
								}

						}
					} else if (!(sender instanceof Player)) {
						p.sendMessage(ChatColor.translateAlternateColorCodes('&',
								"" + Main.getInstance().getConfig().getString("prefix")
										+ " &cYou need to be a player to do this!"));
					}
				}

		// Reloads config

		if (cmd.getName().equalsIgnoreCase("banditdust")) {
			if (sender.hasPermission("dust.reload") || sender.isOp()) {
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', ""
						+ Main.getInstance().getConfig().getString("prefix") + " &aReloading the BanditDust config!"));
				Bukkit.getPluginManager().getPlugin("BanditDust").reloadConfig();
			}
		}

		return false;
	}

	boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException ex) {
			return false;
		}
	}

}
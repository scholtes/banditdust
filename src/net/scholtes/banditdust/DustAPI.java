package net.scholtes.banditdust;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class DustAPI {

	public static int numd;
	public static List<String> cfglore;
	
	public static World w = Bukkit.getWorld(Main.getInstance().getConfig().getString("world"));
	public static WorldGuardPlugin wgd = Main.wg;
	public static RegionManager manager = wgd.getRegionManager(w);

	public static void addDust(int extradust, PlayerDropItemEvent e) {

		Player p = e.getPlayer();
		ItemStack i = e.getItemDrop().getItemStack();
		List<String> lore = i.getItemMeta().getLore();
		File data = new File("plugins/BanditDust/", "items.yml");
		FileConfiguration cfg = YamlConfiguration.loadConfiguration(data);
		int counter = cfg.getConfigurationSection("items").getKeys(false).size();
		for (int count = 1; count <= counter; count++) {
			ItemStack cfgitem = cfg.getItemStack("items." + count + ".itemstack");
			cfglore = cfgitem.getItemMeta().getLore();
			if (lore != null && lore.equals(cfglore)) {
				numd = count;
				break;
			}
		}
		if (lore != null && lore.equals(cfglore)) {
			String min = cfg.getString("items." + numd + ".min");
			String max = cfg.getString("items." + numd + ".max");
			int random1 = ThreadLocalRandom.current().nextInt(Integer.parseInt(min), Integer.parseInt(max) + 1);
			int random2 = random1 + extradust;
			int random = random2 * i.getAmount();
			if (cfg.getString("items." + numd + ".type").equals("pickaxe")) {
				ItemStack dust = new ItemStack(Material.SULPHUR, random);
				ItemMeta meta = dust.getItemMeta();
				List<String> dustlore = new ArrayList<String>();
				dustlore.add(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("pickaxedustlore")));
				meta.setLore(dustlore);
				meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("pickaxedustname")));
				dust.setItemMeta(meta);
				Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
					public void run() {
						if (e.getItemDrop().isOnGround()) {
							Location location = e.getItemDrop().getLocation();
							ProtectedRegion region = manager
									.getRegion(Main.getInstance().getConfig().getString("region"));
							if (region == null) {
								return;
							}
							if (region.contains(location.getBlockX(), location.getBlockY(), location.getBlockZ())) {
								e.getItemDrop().remove();
								p.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("crushingpickaxe")));
								p.playSound(p.getLocation(), Sound.ANVIL_BREAK, 3.0F, 0.533F);
								Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
									public void run() {
										p.getInventory().addItem(dust);
										p.playSound(p.getLocation(), Sound.LEVEL_UP, 3.0F, 0.533F);
										p.sendMessage(ChatColor.translateAlternateColorCodes('&',
												"" + Main.getInstance().getConfig().getString("pickaxedustget")
														.replace("{amount}", Integer.toString(random))));
									}
								}, 25L);
							}
						}
					}
				}, 25L);
			} else if (cfg.getString("items." + numd + ".type").equals("tag")) {
				ItemStack dust = new ItemStack(Material.REDSTONE, random);
				ItemMeta meta = dust.getItemMeta();
				List<String> dustlore = new ArrayList<String>();
				dustlore.add(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("tagdustlore")));
				meta.setLore(dustlore);
				meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("tagdustname")));
				dust.setItemMeta(meta);
				Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
					public void run() {
						if (e.getItemDrop().isOnGround()) {
							Location location = e.getItemDrop().getLocation();
							ProtectedRegion region = manager
									.getRegion(Main.getInstance().getConfig().getString("region"));
							if (region == null) {
								return;
							}
							if (region.contains(location.getBlockX(), location.getBlockY(), location.getBlockZ())) {
								e.getItemDrop().remove();
								p.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("crushingtag")));
								p.playSound(p.getLocation(), Sound.ANVIL_BREAK, 3.0F, 0.533F);
								Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
									public void run() {
										p.getInventory().addItem(dust);
										p.playSound(p.getLocation(), Sound.LEVEL_UP, 3.0F, 0.533F);
										p.sendMessage(ChatColor.translateAlternateColorCodes('&',
												"" + Main.getInstance().getConfig().getString("tagdustget")
														.replace("{amount}", Integer.toString(random))));
									}
								}, 25L);
							}
						}
					}
				}, 25L);
			} else if (cfg.getString("items." + numd + ".type").equals("armor")) {
				ItemStack dust = new ItemStack(Material.GLOWSTONE_DUST, random);
				ItemMeta meta = dust.getItemMeta();
				List<String> dustlore = new ArrayList<String>();
				dustlore.add(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("armordustlore")));
				meta.setLore(dustlore);
				meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("armordustname")));
				dust.setItemMeta(meta);
				Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
					public void run() {
						if (e.getItemDrop().isOnGround()) {
							Location location = e.getItemDrop().getLocation();
							ProtectedRegion region = manager
									.getRegion(Main.getInstance().getConfig().getString("region"));
							if (region == null) {
								return;
							}
							if (region.contains(location.getBlockX(), location.getBlockY(), location.getBlockZ())) {
								e.getItemDrop().remove();
								p.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("crushingarmor")));
								p.playSound(p.getLocation(), Sound.ANVIL_BREAK, 3.0F, 0.533F);
								Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
									public void run() {
										p.getInventory().addItem(dust);
										p.playSound(p.getLocation(), Sound.LEVEL_UP, 3.0F, 0.533F);
										p.sendMessage(ChatColor.translateAlternateColorCodes('&',
												"" + Main.getInstance().getConfig().getString("armordustget")
														.replace("{amount}", Integer.toString(random))));
									}
								}, 25L);
							}
						}
					}
				}, 25L);
			} else if (cfg.getString("items." + numd + ".type").equals("scroll")) {
				ItemStack dust = new ItemStack(Material.SUGAR, random);
				ItemMeta meta = dust.getItemMeta();
				List<String> dustlore = new ArrayList<String>();
				dustlore.add(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("scrolldustlore")));
				meta.setLore(dustlore);
				meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("scrolldustname")));
				dust.setItemMeta(meta);
				Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
					public void run() {
						if (e.getItemDrop().isOnGround()) {
							Location location = e.getItemDrop().getLocation();
							ProtectedRegion region = manager
									.getRegion(Main.getInstance().getConfig().getString("region"));
							if (region == null) {
								return;
							}
							if (region.contains(location.getBlockX(), location.getBlockY(), location.getBlockZ())) {
								e.getItemDrop().remove();
								p.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("crushingscroll")));
								p.playSound(p.getLocation(), Sound.ANVIL_BREAK, 3.0F, 0.533F);
								Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
									public void run() {
										p.getInventory().addItem(dust);
										p.playSound(p.getLocation(), Sound.LEVEL_UP, 3.0F, 0.533F);
										p.sendMessage(ChatColor.translateAlternateColorCodes('&',
												"" + Main.getInstance().getConfig().getString("scrolldustget")
														.replace("{amount}", Integer.toString(random))));
									}
								}, 25L);
							}
						}
					}
				}, 25L);
			} else if (cfg.getString("items." + numd + ".type").equals("pet")) {
				ItemStack dust = new ItemStack(Material.FIREWORK_CHARGE, random);
				ItemMeta meta = dust.getItemMeta();
				List<String> dustlore = new ArrayList<String>();
				dustlore.add(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("petdustlore")));
				meta.setLore(dustlore);
				meta.setDisplayName(ChatColor.translateAlternateColorCodes('&',
						"" + Main.getInstance().getConfig().getString("petdustname")));
				dust.setItemMeta(meta);
				Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
					public void run() {
						if (e.getItemDrop().isOnGround()) {
							Location location = e.getItemDrop().getLocation();
							ProtectedRegion region = manager
									.getRegion(Main.getInstance().getConfig().getString("region"));
							if (region == null) {
								return;
							}
							if (region.contains(location.getBlockX(), location.getBlockY(), location.getBlockZ())) {
								e.getItemDrop().remove();
								p.sendMessage(ChatColor.translateAlternateColorCodes('&',
										"" + Main.getInstance().getConfig().getString("crushingpet")));
								p.playSound(p.getLocation(), Sound.ANVIL_BREAK, 3.0F, 0.533F);
								Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
									public void run() {
										p.getInventory().addItem(dust);
										p.playSound(p.getLocation(), Sound.LEVEL_UP, 3.0F, 0.533F);
										p.sendMessage(ChatColor.translateAlternateColorCodes('&',
												"" + Main.getInstance().getConfig().getString("petdustget")
														.replace("{amount}", Integer.toString(random))));
									}
								}, 25L);
							}
						}
					}
				}, 25L);
			}

		} else {
			ItemStack droppedItem = new ItemStack(e.getItemDrop().getItemStack());
			Bukkit.getScheduler().scheduleSyncDelayedTask(Main.getInstance(), new Runnable() {
				public void run() {
					if (e.getItemDrop().isOnGround()) {
						Location location = e.getItemDrop().getLocation();
						ProtectedRegion region = manager.getRegion(Main.getInstance().getConfig().getString("region"));
						if (region == null) {
							return;
						}
						if (region.contains(location.getBlockX(), location.getBlockY(), location.getBlockZ())) {
							e.getItemDrop().remove();
							p.getInventory().addItem(droppedItem);
						}
					}
				}
			}, 25L);
		}
	}

}